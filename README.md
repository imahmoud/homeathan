# Home Athan

##Version 0.0.2:

This project is intended for playing the Athan five times a day.
You have to clone the repository to your raspberry pi device, and run the installation file.
sudo sh install.sh

The installation file will set up the LCD display, install the dependencies needed and finally
sets up a service under systemd that will keep the software running all the time, also after a reboot.

###Release Notes
1. 0 down time
2. Lcd display ready (Only Linux)
3. Evening duaa
4. Python code
5. Works directly after restart without farther actions
6. Works on both Linux and Windows


##Version: 0.0.1
This project is intended for playing the Athan five times a day.
PHP script reads the data from muslimpro.com
Cron Job is playing the Athan.

- README.md       ---- Read me file.
- alarm.mp3       ---- one verse from holy quran to be played 5 minutes before the next Athan time as a reminder.
- aqsaathan.mp3   ---- This is the acutal athan. Here we use Aqsa athan, you can replace it with your prefered Athan voice.
- playAlarm       ---- This is a bash script that execute/play the alarm file when it times come.
- playAthan.sh    ---- This is a bash script that execute/play the Athan file when it times come.
- prayTime        ---- This is the cron job file that will be written everyday with the new than times for the new day.
- process.php     ---- This is the main process script. It is executed every morning at 03:00am and writes a new cron job file with the new times.
