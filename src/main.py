import datetime
import os
import time

import vlc

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

if os.name != 'nt':
    import drivers
from datetime import date, datetime, timedelta
from os import environ
import pandas as pd
from playsound import playsound

# from pygame import mixer

environ['PYGAME_HIDE_SUPPORT_PROMPT'] = '1'

hijri_date_decoded = ('')


def update_prayer_times_file():
    global hijri_date_decoded
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')

    service = webdriver.ChromeService(executable_path='/usr/bin/chromedriver')
    driver = webdriver.Chrome(service=service, options=chrome_options)

    driver.get("https://mawaqit.net/en/m/al-nour-hamburg")
    wait = WebDriverWait(driver, 10)

    element_date = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="hijriDate"]/span')))
    element_fajer = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[6]/div[1]/div[3]/div')))
    element_shams = wait.until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[7]/div[2]/div[2]/div[2]/div')))
    element_dohur = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[6]/div[2]/div[3]/div')))
    element_aser = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[6]/div[3]/div[3]/div')))
    element_maghrib = wait.until(
        EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[6]/div[4]/div[3]/div')))
    element_ishaa = wait.until(EC.visibility_of_element_located((By.XPATH, '//*[@id="main"]/div[6]/div[5]/div[3]/div')))

    hijri_date = element_date.text
    fajer_time = element_fajer.text
    shams_time = element_shams.text
    dohur_time = element_dohur.text
    aser_time = element_aser.text
    maghrib_time = element_maghrib.text
    ishaa_time = element_ishaa.text
    driver.quit()
    # hijri_date_encoded = hijri_date.encode('utf-8', errors='ignore')
    # hijri_date_decoded = hijri_date_encoded.decode('utf-8')
    # print("Hijri Date (after):", hijri_date_decoded)
    # hijri_date = hijri_date.encode()
    # print('Hijri Date: ' + hijri_date)
    print('Fajer: ' + fajer_time)
    print('Shams: ' + shams_time)
    print('Dohur: ' + dohur_time)
    print('Aser: ' + aser_time)
    print('Maghrib: ' + maghrib_time)
    print('Ishaa: ' + ishaa_time)

    if os.name == 'nt':
        file = open('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\athan.csv', 'a+')
    else:
        file = open('/home/pi/homeathan/resources/athan.csv', 'a+')
    file.write(str(date.today()) + ', ' + fajer_time + ', ' + shams_time + ', '
               + dohur_time + ', ' + aser_time + ', ' + maghrib_time
               + ', ' + ishaa_time + '\n')
    file.close()


def play_athan():
    print('playing athan...')
    if os.name == 'nt':
        playsound('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\aqsaathan.mp3')
    else:
        # sound = mixer.Sound('/home/pi/homeathan/resources/aqsaathan.wav')
        # sound.play()
        instance = vlc.Instance('--no-xlib')
        player = instance.media_player_new()
        media = instance.media_new('/home/pi/homeathan/resources/aqsaathan.wav')
        player.set_media(media)
        player.play()
        time.sleep(60)
    print('finished playing athan...')


def play_alarm():
    print('playing alarm...')
    if os.name == 'nt':
        playsound('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\alarm.mp3')
    else:
        # sound = mixer.Sound('/home/pi/homeathan/resources/alarm.wav')
        # sound.play()

        instance = vlc.Instance('--no-xlib')
        player = instance.media_player_new()
        media = instance.media_new('/home/pi/homeathan/resources/alarm.wav')
        player.set_media(media)
        player.play()
        time.sleep(60)
    print('finished playing alarm...')


def play_morning_duaa():
    print('playing morning duaa...')
    if os.name == 'nt':
        playsound('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\morning_duaa.mp3')
    else:
        # sound = mixer.Sound('/home/pi/homeathan/resources/morning_duaa.wav')
        # sound.play()

        instance = vlc.Instance('--no-xlib')
        player = instance.media_player_new()
        media = instance.media_new('/home/pi/homeathan/resources/morning_duaa.wav')
        player.set_media(media)
        player.play()
        time.sleep(60)
    print('finished playing morning duaa...')


def play_evening_duaa():
    print('playing evening duaa...')
    if os.name == 'nt':
        playsound('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\evening_duaa.mp3')
    else:
        # sound = mixer.Sound('/home/pi/homeathan/resources/evening_duaa.wav')
        # sound.play()
        instance = vlc.Instance('--no-xlib')
        player = instance.media_player_new()
        media = instance.media_new('/home/pi/homeathan/resources/evening_duaa.wav')
        player.set_media(media)
        player.play()

        time.sleep(60)
        time.sleep(300)
    print('finished playing evening duaa...')


def home_athan():
    print('home athan program started...')
    # mixer.init()
    if os.name == 'nt':
        athan_csv = pd.read_csv('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\athan.csv', header=0)
    else:
        display = drivers.Lcd()
        athan_csv = pd.read_csv('/home/pi/homeathan/resources/athan.csv', header=0)
    athan_csv.columns = athan_csv.columns.str.strip()

    fajer = athan_csv.loc[len(athan_csv) - 1, 'fajer'].strip()
    shams = athan_csv.loc[len(athan_csv) - 1, 'shams'].strip()
    duhur = athan_csv.loc[len(athan_csv) - 1, 'duhur'].strip()
    aser = athan_csv.loc[len(athan_csv) - 1, 'aser'].strip()
    maghrib = athan_csv.loc[len(athan_csv) - 1, 'maghrib'].strip()
    isha = athan_csv.loc[len(athan_csv) - 1, 'isha'].strip()
    date_updated = False
    next_prayer = ''
    next_pray_time = ''

    while True:
        today = date.today()
        last_date = pd.to_datetime(athan_csv.loc[len(athan_csv) - 1, 'date']).date()
        if today > last_date:
            print('today is bigger than last date, we have to update...')
            update_prayer_times_file()
            date_updated = True

        if date_updated:
            print('read today reading from csv file...')
            if os.name == 'nt':
                athan_csv = pd.read_csv('C:\\Users\\imahmoud\\PycharmProjects\\homeathan\\resources\\athan.csv',
                                        header=0)
            else:
                athan_csv = pd.read_csv('/home/pi/homeathan/resources/athan.csv', header=0)
            athan_csv.columns = athan_csv.columns.str.strip()
            fajer = athan_csv.loc[len(athan_csv) - 1, 'fajer'].strip()
            shams = athan_csv.loc[len(athan_csv) - 1, 'shams'].strip()
            duhur = athan_csv.loc[len(athan_csv) - 1, 'duhur'].strip()
            aser = athan_csv.loc[len(athan_csv) - 1, 'aser'].strip()
            maghrib = athan_csv.loc[len(athan_csv) - 1, 'maghrib'].strip()
            isha = athan_csv.loc[len(athan_csv) - 1, 'isha'].strip()
            date_updated = False

        now = datetime.now().strftime('%Y-%m-%d %H:%M')
        alarm_time = datetime.now() + timedelta(minutes=5)
        alarm_time = alarm_time.strftime('%Y-%m-%d %H:%M')
        evening_time = datetime.now() + timedelta(minutes=20)
        evening_time = evening_time.strftime('%Y-%m-%d %H:%M')

        fajer_time_str = str(date.today()) + ' ' + fajer
        fajer_time = datetime.strptime(fajer_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        shams_time_str = str(date.today()) + ' ' + shams
        shams_time = datetime.strptime(shams_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        duhur_time_str = str(date.today()) + ' ' + duhur
        duhur_time = datetime.strptime(duhur_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        aser_time_str = str(date.today()) + ' ' + aser
        aser_time = datetime.strptime(aser_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        maghrib_time_str = str(date.today()) + ' ' + maghrib
        maghrib_time = datetime.strptime(maghrib_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        isha_time_str = str(date.today()) + ' ' + isha
        isha_time = datetime.strptime(isha_time_str, '%Y-%m-%d %H:%M').strftime('%Y-%m-%d %H:%M')

        if fajer_time == alarm_time or shams_time == alarm_time or \
                duhur_time == alarm_time or aser_time == alarm_time or \
                maghrib_time == alarm_time or isha_time == alarm_time:
            play_alarm()
        if maghrib_time == evening_time:
            play_evening_duaa()
        if fajer_time == now:
            next_prayer = 'shams'
            next_pray_time = shams_time
            if os.name != 'nt':
                display.lcd_clear()
            play_athan()
        if duhur_time == now:
            next_prayer = 'aser'
            next_pray_time = aser_time
            if os.name != 'nt':
                display.lcd_clear()
            play_athan()
        if aser_time == now:
            next_prayer = 'maghrib'
            next_pray_time = maghrib_time
            if os.name != 'nt':
                display.lcd_clear()
            play_athan()
        if maghrib_time == now:
            next_prayer = 'isha'
            next_pray_time = isha_time
            if os.name != 'nt':
                display.lcd_clear()
            play_athan()
        if isha_time == now:
            next_prayer = 'fajer'
            next_pray_time = fajer_time
            if os.name != 'nt':
                display.lcd_clear()
            play_athan()
        if shams_time == now:
            next_prayer = 'duhur'
            next_pray_time = duhur_time
            if os.name != 'nt':
                display.lcd_clear()
            play_morning_duaa()
        if os.name != 'nt':
            # display.lcd_display_string(str(hijri_date_decoded), 1)
            display.lcd_display_string(next_prayer + ' is at: ', 1)
            display.lcd_display_string(str(next_pray_time), 2)


if __name__ == '__main__':
    home_athan()
